# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

The purpose of this repository is to provide a base checkpoint template for development using Spring + React. 
Feel free to clone the repository and focus on implementing the real code and avoid the boilerplate. 

Happy Coding!

### Requirements 
Java 11

Maven
 
Node 

### How do I get set up? ###

Pull the contents of the repository and build using maven. That should get you started.